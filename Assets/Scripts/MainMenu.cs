﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    public void ButtonMenu(string option)
    {
        switch (option)
        {
            case "new":
                Debug.Log("Starting new game");
                break;
            case "load":
                Debug.Log("Loading previous game");
                break;
            case "settings":
                Debug.Log("Showing settings");
                break;
            case "exit":
                Debug.Log("Exitting game");
                break;
            default:
                Debug.LogError("Unknown option passed: " + option);
                break;
        }
    }

}
